import 'package:flutter/material.dart';

import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:componentes/src/routes/routes.dart';

import 'package:componentes/src/pages/alert_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'componentes APP',
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', ''), // English, no country code
        const Locale('es', 'ES'),
      ], //home: HomePageTemp()
      //home: HomePage()
      initialRoute: '/',
      routes:
          getApplicationRoutes(), //Finalmente nos creamos un metodo que es llamado y contiene de manera organizada las routas de la aplicacion en un archivo idependiente
      onGenerateRoute: (RouteSettings settings) {
        //Sintaxis para generar rutas dinamicamente

        print('Ruta llamda: ${settings.name}');

        return MaterialPageRoute(
            //Retorna un MaterialPageRoute
            builder: (BuildContext context) =>
                AlertPage() //Retorna la pagina por defecto que queremos mostrar
            );
      },
    );
  }
}
