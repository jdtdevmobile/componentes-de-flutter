import 'package:componentes/src/widgets/boton_login.dart';
import 'package:componentes/src/widgets/btn_text_file.dart';
import 'package:flutter/material.dart';

class MostrarWidgetsDeEjemplo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final textEditingController = TextEditingController();
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ButtonTextField(
            icon: Icons.supervised_user_circle,
            placeholder: 'Toro y torito',
            keyboardType: TextInputType
                .emailAddress, //esto es muy importante toro,,, es de que tipo sera la cajonera vale///
            textEditingController: textEditingController,
          ),
            ButtonTextField(
            icon: Icons.supervised_user_circle,
            placeholder: 'Toroo',
            keyboardType: TextInputType
                .emailAddress, //esto es muy importante toro,,, es de que tipo sera la cajonera vale///
            textEditingController: textEditingController,
          ),
            ButtonTextField(
            icon: Icons.supervised_user_circle,
            placeholder: 'Toro y torito',
            keyboardType: TextInputType
                .emailAddress, //esto es muy importante toro,,, es de que tipo sera la cajonera vale///
            textEditingController: textEditingController,
          ),
            ButtonTextField(
            icon: Icons.supervised_user_circle,
            placeholder: 'Toro y torito',
            keyboardType: TextInputType
                .emailAddress, //esto es muy importante toro,,, es de que tipo sera la cajonera vale///
            textEditingController: textEditingController,
          ),
          BotonLogin(),
          BotonLogin(),
          BotonLogin(),
          BotonLogin(),
          BotonLogin(),
        ],
      ),
    );
  }
}
