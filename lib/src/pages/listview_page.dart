import 'package:flutter/material.dart';
import 'dart:async';

class ListaPage extends StatefulWidget {
  @override
  _ListaPageState createState() => _ListaPageState();
}

class _ListaPageState extends State<ListaPage> {
  ScrollController _scrollController = new ScrollController();

  List<int> _listaNumeros = new List();
  int _ultimoItem = 0;
  bool _isLoading = false; //Propiedad que se cambia cada vez que llama al Future fetchData

  @override
  void initState() {
    super.initState();
    _agregar10();

    //  Este metodo verifica si las 10 imagenes ya han sido cargadas en pantalla
    _scrollController.addListener(() {
      //El ScrollControler es un elemento que esta escuchando todos los cambios
      //Esta verificacion de las imagenes mediante la posicion de los pixeles
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        //_agregar10();
        fetchData(); //Luego de validar que esta en el final de la pantalla llama al metodo fetchData (obtener datos) que es un future
      }
    });
  }

  //Se ejecuta cuando la pagina deja de estar en el stack de la pagina
  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Listas'),
        ),
        // Se crea una pila o Stack para poder poner el circulo de carga sobre las imagene ya que la pila me permite pone elementos encima de otros
        body: Stack(
          children: <Widget>[
            _crearLista(),
            _crearLoading() //Este metodo contendra la sintaxis que me crea el circulo indicador de carga de las imagenes.
          ],
        )
    );
  }

//
  Widget _crearLista() {
    return RefreshIndicator( //Este widget debe envolver un elemento que tenga un scroll, en este caso un ListView.builder

          onRefresh: obtenerPagina1, //Hasta este puntoes decir con el llamado de la funcion vacia ((){}) aparece el indicador, pero se queda en pantalla girando y no se elimina
                                     //Luego se hace el llamado del metodo obtenerPagina1. Nota solo es la referencia al metodo, aun no se esta ejecutando.
          child: ListView.builder(
        controller: _scrollController,
        itemCount: _listaNumeros.length,
        itemBuilder: (BuildContext context, int index) {
          final imagen = _listaNumeros[index];

          return FadeInImage(
            placeholder: AssetImage('assets/jar-loading.gif'),
            image: NetworkImage('https://picsum.photos/500/300/?image=$imagen'),
          );
        },
      ),
    );
  }

  //Este metodo contendra la sintaxis que permite tener la logica adecuada del indicador de refresco y debe ir dentro de  un future que devuelva vacion

  Future <Null> obtenerPagina1() async {

    final duration = new Duration ( seconds:2 );
    new Timer( duration, (){
      _listaNumeros.clear(); //Purga la lista de numeros del arreglo.
      _ultimoItem++; //permite obtetener nuevas imagenes al incrementar esta variable o propiedad
      _agregar10(); //agrega 10 nuevas imagenes para cargar 
    });

    return Future.delayed(duration) ;
  }


  void _agregar10() {
    for (var i = 1; i < 10; i++) {
      _ultimoItem++;
      _listaNumeros.add(_ultimoItem);
    }

    setState(() {});
  }

  Future<Null> fetchData() async {
    _isLoading = true;
    setState(() {}); // preguntar porque la siguiente sintaxis no esta dentro del setState? no comprendo funcionamiento de este

    final duration = new Duration(seconds: 2);
    new Timer( duration, respuestaHTTP ); //al pasar 2 segundos se envia la referencia al metodo respuestaHTTP. Preguntar ? por la referencia en este constructor al metodo
  }

  //Cunado ya este la respuesta en respuestaHTTP puedo mover el scroll con la siguiente sintaxis dentro de este metodo.
  void respuestaHTTP() {
    _isLoading = false; //Se pone en false porque en esta linea ya han pasado los 2 segundos y significa que ya cargo

    _scrollController.animateTo(
        _scrollController.position.pixels + 100, //Al llegar al final de las imagenes muestra una pequeña parte de las siguientes imagenes que han sido cargadas
        curve: Curves.fastOutSlowIn, //Es el tipo de animacion con el que se cargara las imagenes.
        duration: Duration(milliseconds: 250)
     );

    _agregar10();
  }

//Contiene la sintaxis para crear el circulo de progreso sobre las imagenes en la pantalla
  Widget _crearLoading() {
    if (_isLoading) {
      return Column(
        // Se ubico dentro de una columna y luego dentro de un row para que permitiera ubicarse ne la parte inferior central
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
            ],
          ),
          SizedBox(height:15.0) //Con esta propiedad aleja un poco en el circulo de carga del borde inferior
        ],
      );
    } else {
      return Container();
    }
  }
}
