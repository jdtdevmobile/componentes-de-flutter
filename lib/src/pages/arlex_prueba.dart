import 'package:componentes/src/models/usuario_model.dart';
import 'package:flutter/material.dart';

class Usuarios extends StatefulWidget {
  @override
  _UsuariosState createState() => _UsuariosState();
}

class _UsuariosState extends State<Usuarios> {
final usuarios = [

  Usuario(uid:'1', email: 'lsksk', name: 'toro', online: true),
  Usuario(uid:'2', email: 'hgfdk', name: 'roko', online: true),
  Usuario(uid:'3', email: 'luuuyy', name: 'nombre', online: true),
  Usuario(uid:'4', email: 'lsksk', name: 'tabarese', online: true),
  Usuario(uid:'5', email: 'lsksk', name: 'nombre', online: true),
];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hola Mundo')
      ),
      body: _listaUsuarios(),
    );
  }

  ListView _listaUsuarios() {
    return ListView.separated(
      itemBuilder: (_, i) => ListTile(
        title: Text(usuarios[i].name),
      ) , 
      separatorBuilder: (_,i) => Divider(), 
      itemCount: usuarios.length
      );
  }
}