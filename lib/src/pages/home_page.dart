import 'package:flutter/material.dart';

import 'package:componentes/src/providers/menu_provider.dart';

import 'package:componentes/src/utils/icono_string_util.dart';



class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
      ),
      body: _lista(),
    );
  }

  
  Widget _lista() {
    
  return FutureBuilder( //Este Future  Builer me cosntruye la lista basado en los datos del archivo Json que son leidoa con aterioridad a que se imprima la lista de widgets
    future: menuProvider.cargarData(),
    initialData: [],
    builder: ( context, AsyncSnapshot<List<dynamic>> snapshot){

      //print('builder');
  
      
        return ListView(
          children: _listaItems(snapshot.data, context), //Preguntar por la transicion de este context (para navegar)
        );
    },    
  );

  //print( menuProvider.opciones );
  //menuProvider.cargarData().then((opciones){

    //print( '_lista' );
    //print( opciones );

  }//});

  List<Widget> _listaItems(List<dynamic> data, BuildContext context){ //Context por parametro para (navegar)
     //return [
       // ListTile( title: Text('Hola mundo') ),
        //Divider(),
        //ListTile( title: Text('Hola mundo') ),
        //Divider(),
        //ListTile( title: Text('Hola mundo') ),
      //];
    final   List<Widget> opciones = []; //lista de widgets de nombre opciones que esta contenido en un arreglo.

    data.forEach((opt) {

      final widgetTemp = ListTile(//Esto es una lista de ADORNOS
        title: Text(opt ['texto'] ),
       // leading: Icon(Icons.account_circle, color: Colors.blue ),
       //Aqui el metodo getIcon()recibe una clave y retornara el valor
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: (){

          Navigator.pushNamed(context, opt['ruta'] );
          
         // final route = MaterialPageRoute( //Variable route para navegar en x pagina 
           // builder: ( context ) => AlertPage()  
          //);
          
          //Navigator.push(context, route); //Esto es para navegar de una pagina a otra el context contiene muchos elementos de la aplicacion

        },
      ); 

      opciones..add( widgetTemp )
              ..add(Divider());

    });

  return opciones;
    

  }

}