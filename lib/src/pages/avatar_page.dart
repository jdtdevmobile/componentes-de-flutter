import 'package:flutter/material.dart';


class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar Page'),
          actions: <Widget>[ 
              Container(
                padding: EdgeInsets.all(5.0),
                child: CircleAvatar(
                  backgroundImage: NetworkImage('https://mysticmask.files.wordpress.com/2013/04/20130411-115114.jpg'),
                  radius: 20.0,
                ),
              ),

              Container(
                margin: EdgeInsets.only(right:15.0),
                child: CircleAvatar(
                  child: Text('Tx'),
                  backgroundColor: Colors.brown,
                ),
              )
          ],
      ),
      body: Center(
        child: FadeInImage(
          image: NetworkImage('https://www.conquermaths.com/news/images/images/Tesla.jpeg'),
          placeholder: AssetImage('assets/jar-loading.gif'),
          fadeInDuration: Duration(milliseconds: 200),
        ),
      )  
    );
  }
}