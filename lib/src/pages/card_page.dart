import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CardPage'),
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2(),
           SizedBox(height: 30.0),
            _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2(),
           SizedBox(height: 30.0),
            _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2(),
           SizedBox(height: 30.0),
            _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2(),
           SizedBox(height: 30.0),
            _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2(),
           SizedBox(height: 30.0),
            _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2(),
           SizedBox(height: 30.0),
          
        ],
      )
    );
  }

  Widget _cardTipo1(){

    return Card(
      elevation:  10.0, //Permite poner una sombra al borde 
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0) ), //Pone circular las esquinas 
      child: Column(
        children: <Widget> [
          ListTile(
            leading: Icon(Icons.photo_album, color: Colors.blue),
            title:  Text('Soy el titulo de esta tarjeta'),
            subtitle: Text( 'Aqui estamos con la descripcion de la tarjeta que quiero que usteds vean aca'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget> [
              FlatButton(
                child: Text('Cancelar'),
                onPressed: (){}
                ),
              FlatButton(
                child:  Text('Ok'),
                onPressed: (){},
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo2(){

    //return Card( Se cambia este return por una variable card que va a contener todo el Container para poder 
    //poner al final de la tarjeta un return con el container y en el toda la personalizacion de la tarjeta
      final card = Container(
      child: Column(
        children: <Widget>[

          FadeInImage(
            image: NetworkImage('https://wallpapercave.com/wp/7eKbNsz.jpg'), //Hace lectura de una imagen en internet
            placeholder: AssetImage('assets/jar-loading.gif'), //Lee un gift de precarga que es un recurso local creado y guardado en la carpeta assets en el proyecto
            fadeInDuration: Duration( milliseconds:200), //Me define el tiempo que tarfa el git de precarga en mostrarse
            height: 300, 
            fit: BoxFit.cover, //Permite que la image se adapte a todo en ancho posible.
          ),

          //Image(
           // image: NetworkImage('https://wallpapercave.com/wp/7eKbNsz.jpg') ,
          //),
          Container(
            padding: EdgeInsets.all(10.0), // Es la distancia de todos los bordes a su al rededor con el limite de la pantalla
            child: Text('Que debe ir aca pues no hay idea')//Texto que lleva en la parte inferior de la imagen
          )
        ]
      )
    );


    return Container(
      
      decoration: BoxDecoration(
         borderRadius: BorderRadius.circular(30.0),
         color: Colors.white,
         boxShadow: <BoxShadow>[
          BoxShadow(
            color:Colors.black26,
            blurRadius: 10.0,
            spreadRadius: 2.0,
            offset: Offset(2.0, 10.0)  
          )
        ]
      ),
      
      //child: card, quita este card en child y lo reemplaza por ClipRrect
      child: ClipRRect( //Esto me recorta la imagen al tamaño del container
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }

}
