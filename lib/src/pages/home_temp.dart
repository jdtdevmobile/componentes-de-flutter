import 'package:flutter/material.dart';


class HomePageTemp extends StatelessWidget {

  final opciones = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco']; //Variable opciones con elementos a mostrar en el ListView
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes Temp'),
      ),
      body: ListView(
        //children: _crearItems() //En el children creamos una funcion de tipo Lista de widgets
        children: _crearItemsCorta()
      ),
    );
  }



  List<Widget> _crearItems() { // Esta funcion regresara una lista para mostrar en el List view


    List<Widget> lista = new List<Widget>(); //Me crea una lista de Widgets  nueva

    for (String opt in opciones) { // Este for me recorre los elementos de la lista opciones

      final tempWidget = ListTile( // Esta variable  me crea un widget temporal
        title: Text( opt ),
      );
      
      lista..add( tempWidget )
            ..add( Divider() );
      
    }

      
    return lista;
  }

  List<Widget> _crearItemsCorta() { //Esto es una lista de widgets y metodo alternativo utilizando el map para listas 

    var widgets = opciones.map( ( item )  { //Regresa una nueva lista que pasa por la funcion del metodo map

      return Column(
        children: [
          ListTile( 
            title: Text( item + '!!!'),
            subtitle: Text('Cualquier cosa'),
            leading: Icon(Icons.account_balance),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){ },
          ),
          Divider()
        ],
      );

    }).toList();

    return widgets;
  




  }



}