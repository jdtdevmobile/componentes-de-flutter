class Usuario {
  final String uid;
  final String name;
  final String email;
  final bool online;

  Usuario({this.email, this.name, this.online, this.uid});
}
