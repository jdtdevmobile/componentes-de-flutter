// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

class BotonLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton.icon(
      // child: Text('Boton Login',style: TextStyle(color: Colors.blue[300], fontSize: 18, fontWeight: FontWeight.bold)),
      shape: StadiumBorder(),
      icon: Icon(Icons.supervised_user_circle),
      label: Text(
        'pepito',
        style: TextStyle(
            color: Colors.blue, fontSize: 18, fontWeight: FontWeight.bold),
      ),
      onPressed: () {},
    );
  }
}
