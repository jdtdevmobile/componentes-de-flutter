import 'package:flutter/material.dart';

class ButtonTextField extends StatelessWidget {
  final IconData icon;
  final String placeholder;
  final TextEditingController textEditingController;
  final TextInputType keyboardType;
  final bool ispassword;

  const ButtonTextField({
    Key key,
    @required this.icon,
    @required this.placeholder,
    @required this.textEditingController,
    this.keyboardType = TextInputType.text,
    this.ispassword = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      padding: EdgeInsets.only(top: 5, left: 5, bottom: 5, right: 50),
      child: TextField(
        controller: this.textEditingController,
        keyboardType: this.keyboardType,

        decoration: InputDecoration(
            prefixIcon: Icon(this.icon),
            hintText: this.placeholder,
            focusedBorder: InputBorder
                .none, //focusBorder elimina la linea inferior del TextField
            border: InputBorder
                .none //remata cualquier vision de linea del TextField

            ), //decoracion de la parte interna del textField
      ),
      decoration: BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.circular(30),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              offset: Offset(0, 5),
            )
          ]),
    );
  }
}
