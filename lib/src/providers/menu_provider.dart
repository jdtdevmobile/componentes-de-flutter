import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class _MenuProvider {

  List<dynamic> opciones = [];

  _MenuProvider(){
    //cargarData();
  }

   Future<List<dynamic>> cargarData() async {

    final resp = await rootBundle.loadString('data/menu_opts.json');
     

        Map dataMap = json.decode( resp ); //Me convierte el String de la Data en un Map para poder usarlo
        //print( dataMap ['rutas'] );
        opciones = dataMap['rutas']; //En esta variable accedemos a los valores de la clave en el map

      return opciones;
      
  }

}

final menuProvider = new _MenuProvider();


