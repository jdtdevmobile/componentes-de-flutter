import 'package:flutter/material.dart';

final _icons = < String, IconData >{  //Esta sintaxis es para definir un mapa 


  'add_alert'     :  Icons.add_alert,
  'accessibility' :  Icons.accessibility,
  'folder_open'   :  Icons.folder_open,
  'donut_large'   :  Icons.donut_large,
  'input'         :  Icons.input,
  'tune'          :  Icons.tune,
  'list'          :  Icons.list,

};                          



//En este metodo el parametro nombre define la clave del mapa anterior y retorna el valor de esa
Icon getIcon ( String nombre) {
  return Icon( _icons[nombre], color: Colors.blue );

}